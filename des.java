public class Student1 {
    String collegeName;
    int collegeCode;
    String fullName;
    double semPercentage;

    Student1() {
        collegeName = "MVGR";
        collegeCode = 33;
    }

    Student1(String fName, double sper) {
        this.fullName = fName;
        this.semPercentage = sper;
    }

    protected void finalize() {
        System.out.println(" DEAD ");
    }

    public static void main(String[] args) {
        Student1 obj1 = new Student1();
        System.out.println(obj1.collegeName + " " + obj1.collegeCode);
        Student1 obj2 = new Student1("Indu", 46);
        System.out.println(obj2.fullName + " " + obj2.semPercentage);
    }
}
