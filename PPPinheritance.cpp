#include<iostream>
using namespace std;
class parent{
public:
int a=20;
int b=30;
void add(){
cout<<"The sum is : "<<a+b<<endl;
}
};
class publicChild : public parent{
public:
void displayPublic(){
cout<< "I am access to everyone"<<endl;
}
};
class privateChild : private parent{
public:
void displayPrivate(){
cout<<"I am access to only my members"<<endl;
}
};
class protectedChild : protected parent{
public:
void displayProtected(){
cout<<"I am access to my class members"<<endl;
}
};
int main(){
publicChild obj1;
privateChild obj2;
protectedChild obj3;
cout<<"public child : "<<endl;
obj1.add();
obj1.displayPublic();
cout<<"private child : "<<endl;
//obj2.add();
obj2.displayPrivate();
cout<<"protected child :"<<endl;
//obj3.add();
obj3.displayProtected();
}
