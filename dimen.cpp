#include<iostream>
using namespace std;
class Box{
    public:
    void boxArea(float length, float width,float height)
    {
        cout<<"The Area Of The Box Is:"<<endl;
        cout<<(2*(length*width)+2*(length*height)+2*(height*width))<<endl;
    }
    void boxVolume(float length, float width, float height);
    friend void displayBoxDimensions();
    inline void displayWelcomeMessage();
};
void displayBoxDimensions(float length, float width, float height)
{
    cout<<"The Dimensions Of The Box Are:"<<endl;
    cout<<"Length:"<<length<<endl;
    cout<<"Width:"<<width<<endl;
    cout<<"Height:"<<height<<endl;
}
inline void Box::displayWelcomeMessage()
{
    cout<<"Hello Welcome To My Program"<<endl;
}
void Box::boxVolume(float length, float width, float height)
{
    cout<<"The Volume Of The Box Is:"<<endl;
        cout<<(length*width*height)<<endl;
}
int main()
{
    Box o;
    float length,width,height;
    o.displayWelcomeMessage();
    cout<<"Enter The Length Width And Height Of The Box"<<endl;
    cin>>length>>width>>height;
    o.boxArea(length,width,height);
    o.boxVolume(length,width,height);
    displayBoxDimensions(length,width,height);
}
