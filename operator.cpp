#include <iostream>
using namespace std;
class Operator {
private:
    int real_num, img_num;

public:
    Operator(int r = 0, int i = 0)
    {
        real_num = r;
        img_num = i;
    }

    void print() 
    { 
        cout << real_num << " + i" << img_num << endl; 
    }

Operator operator*(const Operator& new_op)
{
    Operator result;
    result.real_num = real_num * new_op.real_num;
    result.img_num = img_num * new_op.img_num;
    return result;
}
};

int main()
{
    Operator op1(10, 5), op2(2, 4);
    Operator op3= op1 * op2; 
    op3.print();
    return 0;
}

