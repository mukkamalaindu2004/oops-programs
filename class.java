class Student {
    String fullName;
    int rollNum;
    double semPerentage;
    String collegeName;
    int collegeCode;

    public Student(String fn, int rn, double sp, String cn, int cc) {
        fullName = fn;
        rollNum = rn;
        semPerentage = sp;
        collegeName = cn;
        collegeCode = cc;
    }

    public static void main(String[] args) {
        Student obj = new Student("Indu", 27, 9.24, "MVGR", 33);
        System.out.println(obj.fullName);
        System.out.println(obj.rollNum);
        System.out.println(obj.semPerentage);
        System.out.println(obj.collegeName);
        System.out.println(obj.collegeCode);
    }
}
