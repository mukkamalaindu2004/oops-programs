#include <iostream>
using namespace std;
class Shape {
   public:
    void setWidth(int w)
     {
       width = w;
     }
    void setHeight(int h)
     {
       height = h; 
     }
   protected:
    int width;
    int height;
};
class Rectangle : public Shape {
   public:
    int getArea()
     {
       return (width * height);
     }
};
class Square : public Rectangle {
   public:
    int getPerimeter() 
    { 
      return (2 * (width + height)); 
    }
};
class Triangle : public Shape {
   public:
    int getArea() 
    {
       return (width * height / 2); 
    }
};
int main() {
    Rectangle rect;
    rect.setWidth(5);
    rect.setHeight(7);
    cout << "Single Inheritance Example:" << endl;
    cout << "The area of the rectangle is: " << rect.getArea() << endl << endl;
    Square sq;
    sq.setWidth(4);
    sq.setHeight(4);
    cout << "Multilevel Inheritance Example:" << endl;
    cout << "The area of the square is: " << sq.getArea() << endl;
    cout << "The perimeter of the square is: " << sq.getPerimeter() << endl << endl;
    Triangle tri;
    tri.setWidth(6);
    tri.setHeight(8);
    cout << "Hierarchical Inheritance Example:" << endl;
    cout << "The area of the triangle is: " << tri.getArea() << endl;
    return 0;
}

