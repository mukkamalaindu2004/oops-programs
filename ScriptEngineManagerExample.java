import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import java.util.List;

public class ScriptEngineManagerExample {
    public static void main(String[] args) {
        ScriptEngineManager manager = new ScriptEngineManager();

        // Get the list of available script engines
        List<ScriptEngineFactory> engineFactories = manager.getEngineFactories();
        System.out.println("Available Script Engines:");
        for (ScriptEngineFactory factory : engineFactories) {
            System.out.println("Engine Name: " + factory.getEngineName());
            System.out.println("Engine Version: " + factory.getEngineVersion());
            System.out.println("Language Name: " + factory.getLanguageName());
            System.out.println("Language Version: " + factory.getLanguageVersion());
            System.out.println("Extensions: " + factory.getExtensions());
            System.out.println("Mime Types: " + factory.getMimeTypes());
            System.out.println("Names: " + factory.getNames());
            System.out.println();
        }

        // Get a script engine by name
        ScriptEngine engineByName = manager.getEngineByName("JavaScript");
        if (engineByName != null) {
            System.out.println("Engine found by name: " + engineByName.getFactory().getEngineName());
        } else {
            System.out.println("Engine not found by name");
        }

        // Get a script engine by extension
        ScriptEngine engineByExtension = manager.getEngineByExtension("js");
        if (engineByExtension != null) {
            System.out.println("Engine found by extension: " + engineByExtension.getFactory().getEngineName());
        } else {
            System.out.println("Engine not found by extension");
        }

        // Get a script engine by MIME type
        ScriptEngine engineByMimeType = manager.getEngineByMimeType("text/javascript");
        if (engineByMimeType != null) {
            System.out.println("Engine found by MIME type: " + engineByMimeType.getFactory().getEngineName());
        } else {
            System.out.println("Engine not found by MIME type");
        }
    }
}
